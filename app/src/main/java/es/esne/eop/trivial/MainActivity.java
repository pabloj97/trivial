package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import es.esne.eop.trivial.questions.QuestionDB;
import es.esne.eop.trivial.questions.QuestionModel;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    ImageButton botonNP;
    ImageButton botonSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonNP = findViewById(R.id.botonNP);
        botonSalir = findViewById(R.id.botonSalir);


        // Creamos un listener para que si lo pulsamos nos lleve al activity donde empezaremos el juego

        botonNP.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(TAG, "Lanzamos la activity_jugadores");
                Intent i = new Intent(getBaseContext(), JugadoresActivity.class);
                startActivity(i);
            }
        });


        // Creamos un listener para cerrar la aplicación

        botonSalir.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d(TAG, "Salimos de la aplicación");
                Intent i = new Intent(Intent.ACTION_MAIN);
                i.addCategory(Intent.CATEGORY_HOME);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
    }
}
