package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import es.esne.eop.trivial.questions.QuestionDB;
import es.esne.eop.trivial.questions.QuestionModel;

public class PreguntaActivity extends Activity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    TextView pregunta;
    TextView jugador;
    Button respuesta1;
    Button respuesta2;
    Button respuesta3;
    Button continuar;

    String otroJugador;
    String turnoActual;
    String colorPregunta;
    String estadoPregunta;
    String [] estadoQuesitosJ1;
    String [] estadoQuesitosJ2;

    int respuestaCorrecta;

    private static String[] estadosPregunta = {
            "sin responder",
            "fallada",
            "acertada"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pregunta);

        estadoPregunta = estadosPregunta[0];

        Intent i = getIntent();
        Log.d(TAG, "Obtenemos el intent");
        otroJugador = i.getStringExtra("otro jugador");
        turnoActual = i.getStringExtra("turno");
        estadoQuesitosJ1 = i.getStringArrayExtra("quesitoJ1");
        estadoQuesitosJ2 = i.getStringArrayExtra("quesitoJ2");
        jugador = findViewById(R.id.jugador);

        jugador.setText(turnoActual + ", ");
        pregunta = findViewById(R.id.pregunta);
        respuesta1 = findViewById(R.id.respuesta1);
        respuesta2 = findViewById(R.id.respuesta2);
        respuesta3 = findViewById(R.id.respuesta3);
        continuar = findViewById(R.id.botonContinuar2);

        colorPregunta = i.getStringExtra("colorPregunta");

        respuestaCorrecta = setPregunta();

        checkRespuestas();

        // Comprabación de que hayamos respondido

        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(estadoPregunta != estadosPregunta[0]){
                    Intent intent = new Intent(getBaseContext(), JuegoActivity.class);
                    intent.putExtra("primeraVez", false);
                    if(estadoPregunta == estadosPregunta[1]){
                        intent.putExtra("respuesta", false);
                        intent.putExtra("jugador", otroJugador);
                        intent.putExtra("otro jugador", turnoActual);
                        intent.putExtra("quesitoJ1", estadoQuesitosJ2);
                        intent.putExtra("quesitoJ2", estadoQuesitosJ1);
                    }
                    else{
                        intent.putExtra("respuesta", true);
                        intent.putExtra("jugador", turnoActual);
                        intent.putExtra("otro jugador", otroJugador);
                        intent.putExtra("quesitoJ1", estadoQuesitosJ1);
                        intent.putExtra("quesitoJ2", estadoQuesitosJ2);
                    }
                    intent.putExtra("colorPregunta", colorPregunta);

                    startActivity(intent);
                }
            }
        });
    }

    /**
     * Función que se encarga de comprobar que la respuesta seleccionada sea la correcta.
     * Si lo es, el estado de la pregunta se cambia a "acertada". Si no, a "fallada".
     * Además se hace visible el boton de continuar para volver a JuegoActivity.
     */
    private void checkRespuestas() {
        respuesta1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(estadoPregunta == estadosPregunta[0] && respuestaCorrecta == 1){
                    estadoPregunta = estadosPregunta[2];
                    Toast.makeText(getBaseContext(), "Correcto!", Toast.LENGTH_SHORT).show();
                    continuar.setVisibility(View.VISIBLE);
                }else
                if(estadoPregunta == estadosPregunta[0] && respuestaCorrecta != 1){
                    estadoPregunta = estadosPregunta[1];
                    Toast.makeText(getBaseContext(), "Incorrecto!", Toast.LENGTH_SHORT).show();
                    continuar.setVisibility(View.VISIBLE);
                }
            }
        });
        respuesta2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(estadoPregunta == estadosPregunta[0] && respuestaCorrecta == 2){
                    estadoPregunta = estadosPregunta[2];
                    Toast.makeText(getBaseContext(), "Correcto!", Toast.LENGTH_SHORT).show();
                    continuar.setVisibility(View.VISIBLE);
                }else
                if(estadoPregunta == estadosPregunta[0] && respuestaCorrecta != 2){
                    estadoPregunta = estadosPregunta[1];
                    Toast.makeText(getBaseContext(), "Incorrecto!", Toast.LENGTH_SHORT).show();
                    continuar.setVisibility(View.VISIBLE);
                }
            }
        });
        respuesta3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(estadoPregunta == estadosPregunta[0] && respuestaCorrecta == 3){
                    estadoPregunta = estadosPregunta[2];
                    Toast.makeText(getBaseContext(), "Correcto!", Toast.LENGTH_SHORT).show();
                    continuar.setVisibility(View.VISIBLE);
                }else
                if(estadoPregunta == estadosPregunta[0] && respuestaCorrecta != 3){
                    estadoPregunta = estadosPregunta[1];
                    Toast.makeText(getBaseContext(), "Incorrecto!", Toast.LENGTH_SHORT).show();
                    continuar.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    /**
     * Función que se encarga de seleccionar la pregunta y las respuestas consecuentes dependiendo
     * del color recibido.
     */
    int setPregunta() {
        switch (colorPregunta)
        {
            case "Azul":
                QuestionModel questionModel = QuestionDB.getRandomQuestion(QuestionModel.Type.BLUE);
                pregunta.setText(questionModel.getStatement());
                respuesta1.setText(questionModel.getAnswer1());
                respuesta2.setText(questionModel.getAnswer2());
                respuesta3.setText(questionModel.getAnswer3());
                return questionModel.getCorrect();
            case "Rosa":
                QuestionModel questionModel2 = QuestionDB.getRandomQuestion(QuestionModel.Type.PINK);
                pregunta.setText(questionModel2.getStatement());
                respuesta1.setText(questionModel2.getAnswer1());
                respuesta2.setText(questionModel2.getAnswer2());
                respuesta3.setText(questionModel2.getAnswer3());
                return questionModel2.getCorrect();
            case "Amarillo":
                QuestionModel questionModel3 = QuestionDB.getRandomQuestion(QuestionModel.Type.YELLOW);
                pregunta.setText(questionModel3.getStatement());
                respuesta1.setText(questionModel3.getAnswer1());
                respuesta2.setText(questionModel3.getAnswer2());
                respuesta3.setText(questionModel3.getAnswer3());
                return questionModel3.getCorrect();
            case "Marrón":
                QuestionModel questionModel4 = QuestionDB.getRandomQuestion(QuestionModel.Type.BROWN);
                pregunta.setText(questionModel4.getStatement());
                respuesta1.setText(questionModel4.getAnswer1());
                respuesta2.setText(questionModel4.getAnswer2());
                respuesta3.setText(questionModel4.getAnswer3());
                return questionModel4.getCorrect();
            case "Verde":
                QuestionModel questionModel5 = QuestionDB.getRandomQuestion(QuestionModel.Type.GREEN);
                pregunta.setText(questionModel5.getStatement());
                respuesta1.setText(questionModel5.getAnswer1());
                respuesta2.setText(questionModel5.getAnswer2());
                respuesta3.setText(questionModel5.getAnswer3());
                return questionModel5.getCorrect();
            case "Naranja":
                QuestionModel questionModel6 = QuestionDB.getRandomQuestion(QuestionModel.Type.ORANGE);
                pregunta.setText(questionModel6.getStatement());
                respuesta1.setText(questionModel6.getAnswer1());
                respuesta2.setText(questionModel6.getAnswer2());
                respuesta3.setText(questionModel6.getAnswer3());
                return questionModel6.getCorrect();
        }
        return 0;
    }
}
