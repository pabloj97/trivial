package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class JuegoActivity extends Activity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    TextView turno;
    ImageView quesitos [] = new ImageView[6];
    ImageButton jugar;
    ImageButton volver;
    Button continuar;
    Button salir;
    Button volverAJugar;
    TextView textoGanador;

    private static String[] coloresPreguntas = {
            "Azul",
            "Rosa",
            "Amarillo",
            "Marrón",
            "Verde",
            "Naranja"
    };

    // Variable para comprobar si ya se ha escogido un color
    boolean flag = false;

    // Variable para evitar que se tire el dado más de una vez
    boolean botonPulsado = false;

    // Variable que guarda el jugador del mismo turno
    String turnoActual;

    // Variable que guarda el jugador restante
    String otroJugador;

    // Variable que guarda el color de la pregunta a realizar
    String colorPregunta;

    // Estados de los quesitos del jugador 1
    String[] estadoQuesitosJ1 = {
            "false",
            "false",
            "false",
            "false",
            "false",
            "false"
    };

    // Estados de los quesitos del jugador 2
    String[] estadoQuesitosJ2 = {
            "false",
            "false",
            "false",
            "false",
            "false",
            "false"
    };

    // Variable para comprobar certeza de la respuesta
    boolean respuesta;
    boolean primeraVez;

    boolean ganador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        turno = findViewById(R.id.turno);
        jugar = findViewById(R.id.botonColor);
        volver = findViewById(R.id.botonVolver);
        continuar = findViewById(R.id.botonContinuar1);
        salir = findViewById(R.id.botonSalir1);
        volverAJugar = findViewById(R.id.botonVolverAJugar);
        textoGanador = findViewById(R.id.textoGanador);

        quesitos [0] = findViewById(R.id.qAz);
        quesitos [1] = findViewById(R.id.qR);
        quesitos [2] = findViewById(R.id.qA);
        quesitos [3] = findViewById(R.id.qM);
        quesitos [4] = findViewById(R.id.qV);
        quesitos [5] = findViewById(R.id.qN);

        Log.d(TAG, "Obtenemos el intent para recibir los parámetros");
        Intent intent = getIntent();

        primeraVez = intent.getBooleanExtra("primeraVez", true);

        if(primeraVez){

            primeraVezJuego(intent);

        }else{

            proximasVecesJuego(intent);

        }

        checkQuesitos();

        // Creamos un listener para volver a la pantalla de la selección de nombres
        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), JugadoresActivity.class);
                startActivity(i);
            }
        });
    }

    /**
     * Función que se encarga de realizar todas las funciones por primera vez
     * @param intent
     */
    void primeraVezJuego(Intent intent){
        // Ajustamos la escala de los quesitos para indicar que no tiene ninguno
        for(int i = 0; i < quesitos.length; i++)
        {
            quesitos[i].setScaleX(0.65f);
            quesitos[i].setScaleY(0.65f);
        }

        String j1 = intent.getStringExtra("j1");
        String j2 = intent.getStringExtra("j2");

        turnoActual = setTurno(j1, j2);

        jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!botonPulsado){
                    botonPulsado = true;
                    iaAnimationChoice();
                }
            }
        });

        // Creamos un listener para poder ir al activity que nos muestre la pregunta
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag == true){
                    Log.d(TAG,"Lanzamos un nuevo intent con la pregunta");
                    Intent i = new Intent(getBaseContext(), PreguntaActivity.class);
                    i.putExtra("colorPregunta", colorPregunta);
                    i.putExtra("turno", turnoActual);
                    i.putExtra("otro jugador", otroJugador);
                    i.putExtra("quesitoJ1", estadoQuesitosJ1);
                    i.putExtra("quesitoJ2", estadoQuesitosJ2);
                    startActivity(i);
                }
            }
        });
    }

    /**
     * Función que se encarga de realizar todas las funciones una vez ya se ha comenzado el juego
     * @param intent
     */
    private void proximasVecesJuego(Intent intent) {

        turnoActual = intent.getStringExtra("jugador");
        otroJugador = intent.getStringExtra("otro jugador");
        estadoQuesitosJ1 = intent.getStringArrayExtra("quesitoJ1");
        estadoQuesitosJ2 = intent.getStringArrayExtra("quesitoJ2");
        colorPregunta = intent.getStringExtra("colorPregunta");
        respuesta = intent.getBooleanExtra("respuesta", false);

        turno.setText(turnoActual);
        ajustarEstadoQuesitos();

        jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!botonPulsado && !ganador){
                    botonPulsado = true;
                    iaAnimationChoice();
                }
            }
        });

        // Creamos un listener para poder ir al activity que nos muestre la pregunta
        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag && !ganador){
                    Log.d(TAG,"Lanzamos un nuevo intent con la pregunta");
                    Intent i = new Intent(getBaseContext(), PreguntaActivity.class);
                    i.putExtra("colorPregunta", colorPregunta);
                    i.putExtra("turno", turnoActual);
                    i.putExtra("otro jugador", otroJugador);
                    i.putExtra("quesitoJ1", estadoQuesitosJ1);
                    i.putExtra("quesitoJ2", estadoQuesitosJ2);
                    startActivity(i);
                }
            }
        });
    }

    /**
     * Esta función se encarga de mostrar el tipo de pregunta que es cada color
     */
    private void checkQuesitos() {
        quesitos [0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Geografia", Toast.LENGTH_SHORT).show();
            }
        });
        quesitos [1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Entretenimiento", Toast.LENGTH_SHORT).show();
            }
        });
        quesitos [2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Historia", Toast.LENGTH_SHORT).show();
            }
        });
        quesitos [3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Arte y literatura", Toast.LENGTH_SHORT).show();
            }
        });
        quesitos [4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Ciencia y naturaleza", Toast.LENGTH_SHORT).show();
            }
        });
        quesitos [5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Deporte y ocio", Toast.LENGTH_SHORT).show();
            }
        });


    }

    /**
     * Esta función se encarga de mostrar el tipo de pregunta que es la que ha tocado
     */
    private void mostrarTipoQuesito(String colorPregunta) {
        switch (colorPregunta){
            case "Azul":
                Toast.makeText(getBaseContext(), "Geografia", Toast.LENGTH_SHORT).show();
                break;
            case "Rosa":
                Toast.makeText(getBaseContext(), "Entretenimiento", Toast.LENGTH_SHORT).show();
                break;
            case "Amarillo":
                Toast.makeText(getBaseContext(), "Historia", Toast.LENGTH_SHORT).show();
                break;
            case "Marrón":
                Toast.makeText(getBaseContext(), "Arte y literatura", Toast.LENGTH_SHORT).show();
                break;
            case "Verde":
                Toast.makeText(getBaseContext(), "Ciencia y naturaleza", Toast.LENGTH_SHORT).show();
                break;
            case "Naranja":
                Toast.makeText(getBaseContext(), "Deporte y ocio", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * Función que se encarga de comprobar cuáles son los quesitos que si tenemos y
     * cuáles no.
     */
    void ajustarEstadoQuesitos(){
        int numeroColor;
        switch (colorPregunta){
            case "Azul":
                numeroColor = 0;
                break;
            case "Rosa":
                numeroColor = 1;
                break;
            case "Amarillo":
                numeroColor = 2;
                break;
            case "Marrón":
                numeroColor = 3;
                break;
            case "Verde":
                numeroColor = 4;
                break;
            case "Naranja":
                numeroColor = 5;
                break;
            default:
                numeroColor = 6;
                break;
        }


        for(int i = 0, contador = 0; i < 6; i++)
        {
            if(estadoQuesitosJ1[i].equals("true"))
            {
                contador++;
                quesitos[i].setScaleX(1f);
                quesitos[i].setScaleY(1f);
            }else if(i == numeroColor && respuesta)
            {
                contador++;
                estadoQuesitosJ1[i] = "true";
                quesitos[i].setScaleX(1f);
                quesitos[i].setScaleY(1f);
            }else
            {
                quesitos[i].setScaleX(0.65f);
                quesitos[i].setScaleY(0.65f);
            }
            if(contador == 6){
                Log.d(TAG, "Hay ganador");
                ganador = true;
                textoGanador.setText("¡" + turnoActual + " has ganado!");
                volverAJugar.setVisibility(View.VISIBLE);
                salir.setVisibility(View.VISIBLE);
            }
        }

        volverAJugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Se lanza activity de juego");
                Intent i = new Intent(getBaseContext(), JuegoActivity.class);
                i.putExtra("j1", turnoActual);
                i.putExtra("j2", otroJugador);
                i.putExtra("primeraVez", true);
                startActivity(i);
            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                startActivity(i);
            }
        });
    }

    /**
     * Función que elige aleatoriamente el turno de la primera jugada.
     * Recibe el nombre de los dos jugadores para asignarlo al texto en pantalla.
     * @param j1
     * @param j2
     *
     * Devuelve el jugador que se ha escogido
     * @return
     */
    String setTurno(String j1, String j2){
        Random rand = new Random();
        int n = rand.nextInt(2);

        if(n == 0){
            turno.setText(j1);
            otroJugador = j2;
            return j1;
        }
        else{
            turno.setText(j2);
            otroJugador = j1;
            return j2;
        }
    }

    /**
     * Función que aleatoriamente escoge un color para la pregunta
     */
    void setColorPregunta(){
        // Check para que no pregunten del quesito que ya tienes
        boolean b = true;
        do{
            Random rand = new Random();
            int n = rand.nextInt(6);

            if(estadoQuesitosJ1[n].equals("false")) {
                colorPregunta = coloresPreguntas[n];
                b = false;
            }
        }while(b);

        switch (colorPregunta){
            case "Azul":
                jugar.setBackgroundResource(R.color.quesitoAz);
                break;
            case "Rosa":
                jugar.setBackgroundResource(R.color.quesitoR);
                break;
            case "Amarillo":
                jugar.setBackgroundResource(R.color.quesitoAm);
                break;
            case "Marrón":
                jugar.setBackgroundResource(R.color.quesitoM);
                break;
            case "Verde":
                jugar.setBackgroundResource(R.color.quesitoV);
                break;
            case "Naranja":
                jugar.setBackgroundResource(R.color.quesitoN);
                break;
        }

    }

    /**
     * Función que genera una animación para la elección del color del dado
     */
    void iaAnimationChoice(){
        Handler h = new Handler();
        h.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                jugar.setBackgroundResource(R.color.quesitoV);
                Handler h2 = new Handler();
                h2.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        jugar.setBackgroundResource(R.color.quesitoAz);
                        Handler h3 = new Handler();
                        h3.postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                jugar.setBackgroundResource(R.color.quesitoM);
                                Handler h4 = new Handler();
                                h4.postDelayed(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        jugar.setBackgroundResource(R.color.quesitoAm);
                                        Handler h5 = new Handler();
                                        h5.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                jugar.setBackgroundResource(R.color.quesitoR);
                                                Handler h6 = new Handler();
                                                h6.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        setColorPregunta();
                                                        Log.d(TAG, "Obtenemos el valor del color para la pregunta, " + colorPregunta);
                                                        mostrarTipoQuesito(colorPregunta);
                                                        continuar.setVisibility(View.VISIBLE);
                                                        flag = true;
                                                    }
                                                }, 800);
                                            }
                                        }, 650);

                                    }
                                },500);
                            }
                        },400);
                    }
                },300);
            }
        }, 200);
    }
}
