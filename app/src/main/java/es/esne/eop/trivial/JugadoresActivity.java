package es.esne.eop.trivial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class JugadoresActivity extends Activity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    EditText j1;
    EditText j2;
    Button jugar;
    ImageButton salir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugadores);

        j1 = findViewById(R.id.jugador1);
        j2 = findViewById(R.id.jugador2);

        jugar = findViewById(R.id.botonJugar);
        salir = findViewById(R.id.botonVolver);

        // Creamos un listener para comenzar el juego con la condición
        // de que se hayan introducido los nombres

        jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(String.valueOf(j1.getText()).length() > 0 && j2.getText().length() > 0)
                {
                    Log.d(TAG, "Se lanza activity de juego");
                    Intent i = new Intent(getBaseContext(), JuegoActivity.class);
                    i.putExtra("j1", j1.getText().toString());
                    i.putExtra("j2", j2.getText().toString());
                    i.putExtra("primeraVez", true);
                    startActivity(i);
                }
                else
                    Toast.makeText(JugadoresActivity.this, "Introduce los nombres", Toast.LENGTH_SHORT).show();
            }
        });


        // Creamos un listener para volver al activity inicial

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                startActivity(i);
            }
        });
    }
}
